|****************************************|
|*            Práctica Final            *|
|*              Conecta 4               *|
|*                                      *|
|*  Eugeniu Stina   *    David Soto     *|
|****************************************|

Objetivo 1:
 - El mayor problema era realizar el proceso de comprobación del campo de juego para hayar al ganador.
     Lo que hemos hecho era comprobar después de cada movimiento por fila, columna, diagonales, si hay
     cuatro figuras iguales.
 - Otro problema era el modo de almacenamiento del tablero. Decidimos utilizar un vector de tamaño 
     FILAS x COLUMNAS ya que así tenemos solamente una variable de almacenamiento y iremos comprobando
     si hay ganador mediante cálculo de la posición de las figuras.
 - En el caso del cliente, este tiene pasos previos, cómo registro y login antes de empezar el juego.
 - El jugador sólamente se preocupa en hacer movimientos y ganar/perder. Obviamente el servidor comprueba
     cada vez si el movimiento es legal (hay hueco en la columna, no se elige columna inexistente, etc.)
     
Objetivo 2:
 - Para este objetivo, el servidor añade funcionalidad para poder jugar con un bot. El bot no es una IA
     ni tampoco hace movimientos seguidos (1, 2, 3, etc.). Para ello realizamos un proceso de predefinición
     de los movimientos del bot antes de empezar el juego. No son al azar, son calculados mediante una formula.
 - Cuando el jugador hace un movimiento, dependiendo del último, el bot elige un paso de los precalculados.
 
Objetivo 3:
 - Es el mismo servidor que en objetivo 1 pero añadiendo funcionalidad de visualizar el tablero en el tiempo real.
 - El cliente también tiene esta funcionalidad.
 - Además se representa con una flecha roja la columna utilizada en el último movimiento. 