/*
 * Practica 4: cliente1.c
 * Grupo: rc48
 * Autores:
 * 		Eugeniu Stina
 * 		David Soto
 * Descripcion:
 *		Es el cliente de un juego de conecta4
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <signal.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//  Constantes
#define BUF 2000                                //  Tamaño maximo por cada lectura/escritura en el socket  
#define NombreFichero "DatosCliente.data"        //  Nombre del fichero donde se guardaran los datos del cliente
#define MAX_TAM_ID 12                           //  Tamaño máximo del id
#define TAM_MAX_NAME 16                         //  Tamaño máximo del nombre

//  Variables globales
int sock,c;
char id[MAX_TAM_ID];
char oponentName[TAM_MAX_NAME];
int cols, rows;
char *nombreUser = NULL;
//  Funciones
/*
 *  Intenta establecer conexion con el cliente, si esta registrado hara log in y si no se registrara
 */
void iniciarSesion();
/*
 *  Cierra la conexion con el socket e imprime el error err por pantalla
 */
void cerrar(char* err);
/*
 *  Lee un comando del socket y ejecuta la funcion tratarMensaje
 */
int tratarRespuesta();
/*
 *  Ejecuta las instrucciones necesarias dependiendo del comando de entrada
 */
int tratarMensaje(char *in);
/*
 *  Busca el fichero de datos del cliente y lee el id y el c
 */
int leerDatos();
/*
 *  Guarda los datos del cliente en el fichero
 */
int guardarDatos();
/*
 *  Pregunta al cliente que opcion quiere hacer y envia el comando correspondiente al servidor
 */
int menu();
/*
 */
void eligeColumna();
/*
 */
void stop(int x){
    cerrar(NULL);
}
//  Main
int main(int argc, char *argv[]){
        //Variables	
	int err;
	struct sockaddr_in servidor;
        char buf[BUF];
        
	//Comprobamos argumentos
        if (leerDatos() ==  -1){
            if (argc != 4){
                printf("Numero de argumentos incorrecto.\n");
                printf("Formato: cliente1 <direccion_ip> <puerto> <nombre_usuario>\n");
                exit(-1);
            }
            nombreUser = argv[3];
        } else {
            if (argc != 3){
                printf("Numero de argumentos incorrecto.\n");
                printf("Formato: cliente1 <direccion_ip> <puerto>\n");
                exit(-1);
            }
        }
	//Abrimos el socket
	sock = socket(PF_INET, SOCK_STREAM,0);
	if (sock == -1){
		printf("Error al abrir el socket.\n");
		exit(-1);
	}
	//Rellenamos estructura de la direccion
	servidor.sin_family = AF_INET;
	servidor.sin_port=htons(atoi(argv[2]));
	//inet_aton(direccion_ip_tipo_"127.0.0.1",puntero_a_estructura_en_binario);
	inet_aton(argv[1], &servidor.sin_addr);
	

	//Conexion con el servidor TCP
	err = connect(sock,(struct sockaddr*)&servidor,sizeof(servidor));
	if (err == -1){
		printf("Error de conexion.\n");
		exit(-1);
	}
        //  Gestionamos el Cttrl + C
        signal(SIGINT,(void (*)(int))stop);
        
        //  Iniciamos sesion o nos registramos
        iniciarSesion();
        
        //  Bucle del cliente
        while (1) {
            tratarRespuesta();
        }
}
//
int tratarRespuesta(){
    char *aux;
    char in[BUF];    
    int res = 0;
    memset(in,'\0',BUF);    
    if (read(sock,in,BUF) == -1){
        cerrar("Error en la lectura del socket");
    }    
    aux = strtok(in,"\n");
    while (aux != NULL){
        if (tratarMensaje(aux) == 1)
            res = 1;
        aux = strtok(NULL,"\n");
    }
    return res;
}
//
int tratarMensaje(char *in){
    char *aux;
    char out[BUF];
    
    memset(out,'\0',BUF);
    aux = strtok(in," ");
    if (strcmp(aux,"RESUELVE") == 0){
        int a = atoi(strtok(NULL," "));
        int b = atoi(strtok(NULL," "));
        c = a + b;
        sprintf(out,"RESPUESTA %d\n",c);
        if (write(sock,out,BUF) == -1){
            cerrar("Error al enviar datos al servidor");
        }
        tratarRespuesta();
    } else if (strcmp(aux,"REGISTRADO") == 0) {
        if (strcmp(strtok(NULL," "),"OK") == 0){
            sprintf(id,"%s",strtok(NULL," "));
            printf("ID: %s.\n",id);
            printf("C: %d.\n",c);
            sprintf(out,"SETNAME %s\n",nombreUser);
            write(sock,out,BUF);
            if (guardarDatos() == -1){
                printf("Se ha registrado en el servidor pero no se han podido guardar los datos.\n");
            } else {                
                printf("Se ha registrado correctamente.\n");
            }
        } else {
            cerrar("Error al registrarse en el server");
        }
    } else if (strcmp(aux,"LOGIN") == 0) {
        if (strcmp(strtok(NULL," "),"OK") == 0){
            printf("Conectado al servidor.\n");
        } else {                        
            cerrar("Error al conectarse al servidor.");
        }
    } else if (strcmp(aux,"SETNAME") == 0) {
        if (strcmp(strtok(NULL," "),"OK") == 0){
            printf("Nombre cambiado satisfactoriamente.\n");
        } else {
            printf("Error al cambiar el nombre.\n");
        }
    } else if (strcmp(aux,"GETNAME") == 0) {
        printf("Nombre actual: %s.\n",strtok(NULL," "));
    } else if (strcmp(aux,"LISTADO") == 0) {
        printf("Listado de usuarios conectados: %s.\n",strtok(NULL," "));
    } else if (strcmp(aux,"UPTIME") == 0) {
        int t1 = atoi(strtok(NULL," "));
        int t2 = atoi(strtok(NULL," "));
        printf("\n\tTiempo que lleva el servidor funcionando: %d.\n",t1);
        printf("\tTiempo que llevas conectado: %d.\n",t2);
        printf("-->: ");
        fflush(NULL);
        return 1;
    } else if (strcmp(aux,"START") == 0){
        aux = strtok(NULL, " ");
        if (aux == NULL){
            cerrar("argumento no valido en START");
        }
        sprintf(oponentName,"%s",aux);
        aux = strtok(NULL, " ");
        if (aux == NULL){
            cerrar("argumento no valido en START");
        }
        rows = atoi(aux); 
        aux = strtok(NULL, " ");
        if (aux == NULL){
            cerrar("argumento no valido en START");
        }
        cols = atoi(aux); 
        printf("Juegas contra: %s.\n",oponentName);
        printf("Numero de filas: %d.\n",rows);
        printf("Numero de columnas: %d.\n",cols);        
    } else if (strcmp(aux,"URTURN") == 0){        
        aux = strtok(NULL, "\n");
        if (aux != NULL){
            int colOp = atoi(aux);
            if (colOp >= 1 && colOp <= cols){
                //  Añadir colOp a la tabla
                printf("Tu oponente ha marcado la columna: %d.\n",colOp);
            }  
        }
        eligeColumna();
    } else if (strcmp(aux,"COLUMN") == 0){
        if (strcmp(strtok(NULL," "),"OK") == 0){
            printf("Es el turno de tu oponente.\n");            
        } else {
            printf("Error al elegir la columna.\n");
            eligeColumna();
        }
    } else if (strcmp(aux,"VICTORY") == 0){
        printf("Has ganado.\n");
        cerrar(NULL);
    } else if (strcmp(aux,"DEFEAT") == 0){
        printf("Has perdido.\n");
        cerrar(NULL);
    } else if (strcmp(aux,"TIE") == 0){
        printf("Has empatado.\n");
        cerrar(NULL);
    }
    return 0;
}
//
void cerrar(char* err){
    if (err != NULL){
        printf("Error: %s.\n",err);
    }
    close(sock);    
    exit(0);
}
//
void iniciarSesion(){
    char aux[BUF];
    char *a;
    memset(aux,'\0',BUF);
    read(sock,aux,BUF);
    a = strtok(aux," ");
    if (strcmp(a,"WELCOME\n") == 0){
        printf("Hay sitio en el servidor para jugar.\n");
        if (nombreUser == NULL){
            sprintf(aux,"LOGIN %s %d\n",id,c); 
        } else {
            sprintf(aux,"REGISTRAR\n");            
        }
    } else if (strcmp(a,"FULL") == 0) {
        printf("Servidor lleno. Desconectando.\n");
        cerrar(NULL);
    } else {
        cerrar("Mensaje esperado WELCOME o FULL");
    }  
    write(sock,aux,BUF);
    while (tratarRespuesta() > 0){}     
}
//
int guardarDatos(){
    FILE *datos;
    
    datos = fopen(NombreFichero, "w+");
    if (datos == NULL){
        return -1;
    }
    fprintf(datos,"%s\n",id);
    fprintf(datos,"%d\n",c);    
    fclose(datos);
    return 0;
}

//
int leerDatos(){
    FILE *datos;
    
    datos = fopen(NombreFichero, "r");
    if (datos == NULL){
        return -1;
    }
    fscanf(datos,"%s",id);
    fscanf(datos,"%d",&c);
    fclose(datos);
    return 0;
}
// 
int menu(){
    int r;
    char buf[BUF];
    memset(buf,'\0',BUF);
    do {                
        printf("Elige una opción.\n");
        printf("1 -- Cambiar nombre.\n");
        printf("2 -- Consultar nombre.\n");
        printf("3 -- Listado de clientes conectados.\n");
        printf("4 -- Tiempo servicio.\n");
        
        printf("0 -- Salir.\n");        
        printf("-->: ");
        scanf("%d",&r);
    } while (r < 0 || r > 4);
    switch(r){
        case 0:
            return -1;            
        case 1:
            sprintf(buf,"SETNAME %s\n",nombreUser);
            break;
        case 2:
            sprintf(buf,"GETNAME\n");            
            break;
        case 3:
            sprintf(buf,"LISTA\n");
            break;
        case 4:
            sprintf(buf,"UPTIME\n");
            break;                    
    }
    return write(sock,buf,BUF);    
}

//
void eligeColumna(){
    int columna;
    char buf[BUF];
    
    memset(buf,'\0',BUF);
    printf("Elige el numero de columna: ");
    scanf("%d",&columna);
    sprintf(buf,"COLUMN %d\n",columna);
    
    write(sock,buf,BUF);
}

