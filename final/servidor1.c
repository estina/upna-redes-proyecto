/*
 * Practica FINAL: SERVIDOR1.C
 * Grupo: rc48
 * Autores:
 * 		Eugeniu Stina
 * 		David Soto
 * Descripcion:
 *		Este servidor es de un sencillo juego de “Conecta 4”.
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// Constantes necesarias
#define BUF 2000				// El tamano del buffer
#define MAX_REGISTRADOS 50 		// El numero maximo de clientes registrados
#define MIN_ID 6 				// El tamano minimo del id
#define MAX_ID 12 				// El tamano maximo del id
#define TAM_NOMBRE 16 			// El tamano maximo del nombre del cliente
#define FICHERO "gamers.nfo"	// El nombre del fichero a guardar los datos
#define NGAMERS 2               // El numero de jugadores
#define MIN_ROWS 6
#define MIN_COLS 7
#define SEGUIDOS 4

// Estructura del cliente
	typedef struct cliente_struct {
		char id[MAX_ID];
		int codigo;
		char nombre[TAM_NOMBRE + 1]; 
	} cliente_struct;

// Variables globales
	char buffer[BUF];
	int server_socket;
	int cliente_socket[NGAMERS];
    int pid;
    int rows, cols;
	int *tablero;
	int *check_col;

	int registrados = 0;

	cliente_struct lista_registrados[MAX_REGISTRADOS]; 	// Lista de clientes registrados (NO CONECTADOS!)
    cliente_struct jugador[NGAMERS];

// Funciones necesarias
	void leerclientes();
	void leer_mensaje(int cliente_socket, int num);
	void juego();
	void nuevo();
	
// Terminando correctamente el servidor
	void terminar(int x) {
        kill(pid, SIGKILL);
		// Cerrando conexion
		printf("\nCerrando conexion.\n");
		close(server_socket);
		exit(0);
	}

/*
 * main(): El programa principal.
 */
int main (int argc, char *argv[]) {
	// Variables
	struct sockaddr_in servidor, cliente;
	int len = sizeof(cliente);

	// Comprobamos argumentos
	if (argc != 4){
		printf("Numero de argumentos incorrecto.\n");
		printf("Formato: servidor <puerto> <filas> <columnas>\n");
		exit(-1);
	}

    rows = atoi(argv[2]);
    cols = atoi(argv[3]);

    if (rows < MIN_ROWS | cols < MIN_COLS) {
        printf("Filas minimo %d, columnas minimo %d.\n", MIN_ROWS, MIN_COLS);
		exit(-1);
    }
	// Inicializamos los valores del Tablero y Check_col
	nuevo();
	
	// Abrimos el socket
	if ((server_socket = socket(PF_INET, SOCK_STREAM,0)) == -1){
		printf("Error al abrir el socket.\n");
		exit(-1);
	}

	// Redirigimos la señal SIGINT: Ctrl + C para cerrar correctamente los socket
	struct sigaction stop;
	stop.sa_handler = &terminar;
	stop.sa_flags = 0;
	sigemptyset(&stop.sa_mask);
	sigaction(SIGINT, &stop, NULL); 

	// Rellenamos estructura del servidor (la IP no importa)
	servidor.sin_family = AF_INET;
	servidor.sin_port = htons(atoi(argv[1]));
	servidor.sin_addr.s_addr = INADDR_ANY;
	
	// Reservamos el puerto
	if (bind(server_socket, (struct sockaddr*)&servidor, sizeof(servidor)) == -1){
		printf("Error al reservar el puerto.\n");
		exit(-1);
	}

    printf("Servidor listo. Esperando jugadores\n");

	// Esperando a los clientes
	listen(server_socket, 5);

	while (1) {	

        // Entran NGAMERS jugadores
        for (int i = 0; i < NGAMERS; i++) {

            // Se crea un socket para el cliente entrado
            cliente_socket[i] = accept(server_socket, (struct sockaddr*)&cliente, &len);

            // Si el cliente ha entrado realmente
            if (cliente_socket[i] != -1) {

                write(cliente_socket[i], "WELCOME\n", 9);

                // Leemos la información de los clientes ya guardados
                leerclientes();
        
                // Establecemos el seed del random
                srand(time(NULL));

                leer_mensaje(cliente_socket[i], i);
            }
        }

        // Creamos HIJO para enviar a los demas el mensaje de LLENO
        pid = fork();
        if (pid == 0) {
            while (1) {
                int aux = accept(server_socket, (struct sockaddr*)&cliente, &len);
                if (aux != -1) {
                    write(aux, "FULL\n", 6);                    
                    shutdown(aux, 2);
                }
            }
        }
        // El proceso PADRE sigue con el juego
        else {

            // El proceso es secuencial, cada jugador tiene su turno

            // Enviamos informacion del tamano y nombre del rival
            for (int i = 0; i < NGAMERS; i++) {
                memset(buffer, 0, BUF);
                sprintf(buffer, "START %s %d %d\n", jugador[NGAMERS - i - 1].nombre, rows, cols);

                write(cliente_socket[i], buffer, BUF);
            }

			juego();

        }

		// HACER QUE SE CIERRAN LOS SOCKETS DE CLIENTES Y VOLVER A EMPEZAR TODO
	}
}

/*
 * nuevo(): Reinicia los valores del Tablero y Check_col
 */
	void nuevo() {
		tablero = calloc(rows*cols, sizeof(int));
		check_col = calloc(cols, sizeof(int));

		// Rellenamos el vector de comprobacion de las columnas
		for (int i = 0; i < cols; i++) {
			check_col[i] = rows;
		}
	}

/*
 * leerclientes(): Almacena en la estructura lista_registrados los datos almacenados en el archivo FICHERO
 * 		Necesario para saber los identificadores de todos los clientes para generar nuevos IDs unicos.
 */
	void leerclientes() {
		FILE *fichero;
		int i = 0;
		fichero = fopen(FICHERO, "a+");
		while (fgets(buffer, sizeof(buffer), fichero)) {
			strcpy(lista_registrados[i].id, strtok(buffer, "\t"));
			lista_registrados[i].codigo = atoi(strtok(NULL, "\t"));
			strcpy(lista_registrados[i].nombre, strtok(NULL, "\n"));
			i++;
			memset(buffer, 0, BUF);
		}
		registrados = i;
		fclose(fichero);
	}

/*
 * cerrar(): Se cierra la conexion con el cliente y finaliza el proceso hijo
 */
	void cerrar(int cliente_socket) {
		shutdown(cliente_socket, 2);
		kill(SIGINT,getpid());
	}

/*
 * guardar_clientes(): Cuando se registra un cliente nuevo, todos los datos de todos los clientes se
 * 		almacenan en el archivo FICHERO
 */
	void guardar_clientes() {
		FILE *fichero;
		fichero = fopen(FICHERO,"w");
		for (int i = 0; i < registrados; i++) {
			fprintf(fichero, "%s\t%d\t%s\n", lista_registrados[i].id, lista_registrados[i].codigo, lista_registrados[i].nombre);
		}
		fclose(fichero);
	}

/*
 * unico(): Compara el identificador generado con todos los identificadores almacenados para que no haya
 * 		dos identificadores identicos.
 */
	int unico(char *identidad) {
		for (int i = 0; i < registrados; i++) {
			if (strcmp(lista_registrados[i].id, identidad) == 0)
				return 0;
		}
		return 1;
	}

/*
 * enviar_id(): Genera un identificador comprobando que es unico y lo envia al cliente.
 */
	void enviar_id(int cliente_socket, int num) {
		
		// Generando longitud de ID y otras variables
		int longitud = rand() % MIN_ID + (MAX_ID - MIN_ID);
		char *identidad = calloc(longitud, longitud);

		// Generar ID hasta que no sea unico
		do {
			// Generando el ID de letras minusculas y numeros
			for (int i = 0; i < longitud; i++) {
				if (rand() % 2 == 0)
					identidad[i] = rand() % 10 + 48;
				else
					identidad[i] = rand() % 25 + 97;
			}
		} while (!unico(identidad));
		

		// Enviando mensaje al cliente
		memset(buffer, 0, BUF);
		sprintf(buffer, "REGISTRADO OK %s\n", identidad);
		write(cliente_socket, buffer, BUF);

		// Almacena la información de un nuevo cliente
		strcpy(lista_registrados[registrados].id, identidad);
		strcpy(lista_registrados[registrados].nombre, "Invitado");

        jugador[num] = lista_registrados[registrados++];
	}

/*
 * registrar(): Envia mensajes de comprobacion, solicita generacion del identificador
 * 		y almacena todo en la estructura client. Al finalizar el registro, solicita 
 * 		que se almacene todos los datos en el fichero FICHERO
 */
	int registrar(int cliente_socket, int num) {

		// Generando dos enteros aleatorios de 0 hasta 99
		int a = rand() % 100;
		int b = rand() % 100;

		// Enviamos RESUELVE <a> <b>
		memset(buffer, 0, BUF);
		sprintf(buffer, "RESUELVE %d %d\n", a, b);
		write(cliente_socket, buffer, BUF);

		// RESPUESTA <c>
		memset(buffer, 0, BUF);
		read(cliente_socket, buffer, BUF);

		// Dividimos la cadena en RESPUESTA y <c>
		char *aux = strtok(buffer, " ");

		// Comprobamos si es el mensaje esperado
		if (strcmp(aux, "RESPUESTA") == 0) {
			
			// Comprobamos el valor de <c>
			int c = atoi(strtok(NULL, " "));

			// Si la suma es correcta, registramos al cliente
			if (c == a + b) {

				// Guardamos el numero secreto en la estructura del cliente
				lista_registrados[registrados].codigo = c;
				
				// Enviamos REGISTRADO OK <id>
				enviar_id(cliente_socket, num);

				// Guardamos al cliente en el fichero junto con los demas clientes leidos
				guardar_clientes();
				
				return 1;
				
			}
			// En caso contrario, devolvemos mensaje de error y cerramos la conexion con el cliente
			else {
				memset(buffer, 0, BUF);
				sprintf(buffer, "REGISTRADO ERROR\n");
				write(cliente_socket, buffer, BUF);
			}
		} 
		else // En caso de que la conexion se ha interrumpido
			printf("Error de conexion\n");
		
		return 0;
	}

/*
 * crear_cliente(): Manejador del proceso REGISTRA
 */
	void crear_cliente(int cliente_socket, int num) {

			// Si ha llegado al limite, no permite mas registros
			if (registrados >= MAX_REGISTRADOS) {
				printf("Alcanzado el numero maximo de clientes registrados\n");
				cerrar(cliente_socket);
			}
				
			// Iniciar el proceso de registro
			if (!registrar(cliente_socket, num)) {
				cerrar(cliente_socket);
			} else {
                leer_mensaje(cliente_socket, num);
            }
	}

/*
 * autenticar(): Hace LOGIN con el cliente
 */
	void autenticar(int cliente_socket, int num) {
		char *id = strtok(NULL, " ");
		for (int i = 0; i < registrados; i++) {
			if (!strcmp(lista_registrados[i].id, id)) {
				if (atoi(strtok(NULL, " ")) == lista_registrados[i].codigo) {
					printf("%s conectado\n", lista_registrados[i].nombre);
                                        jugador[num] = lista_registrados[i];
					memset(buffer, 0, BUF);
					sprintf(buffer, "LOGIN OK\n");
					write(cliente_socket, buffer, BUF);

					return;
				}
			}
		}
		memset(buffer, 0, BUF);
		sprintf(buffer, "LOGIN ERROR\n");
		write(cliente_socket, buffer, BUF);
		cerrar(cliente_socket);
	}

/*
 * cambiar_nombre(): Actualiza la lista de clientes registrados, cambia el nombre del cliente y guarda la lista
 */
	void cambiar_nombre(int cliente_socket, int num) {
		// Guardamos el nombre recibido
		char *name = calloc(TAM_NOMBRE, 1);
		strcpy(name, strtok(NULL, " "));                
		// Necesario para eliminar el \n en el nombre
		int len = strlen(name);
		if (len > 0 && name[len-1] == '\n')
			name[--len] = '\0';

		// Eliminamos los elementos sobrantes (El nombre = 16 letras)
		if (len > TAM_NOMBRE) {
			for (int i = len; i >= TAM_NOMBRE; i--)
				name[i] = '\0'; 
		}

		// Leemos lista de clientes desde el fichero
		leerclientes();

		// Buscamos a nuestro cliente dentro de la lista
		for (int i = 0; i < registrados; i++) {
			if (!strcmp(jugador[num].id, lista_registrados[i].id)) {
				strcpy(lista_registrados[i].nombre, name);
                strcpy(jugador[num].nombre, name);
				printf("%s se ha registrado\n", lista_registrados[i].nombre);
				memset(buffer, 0, BUF);
				sprintf(buffer, "SETNAME OK\n");
				write(cliente_socket, buffer, BUF);
				// Guardamos nuestra lista actualizada
				guardar_clientes();

				return;
			}
		}
		memset(buffer, 0, BUF);
		sprintf(buffer, "SETNAME ERROR\n");
		write(cliente_socket, buffer, BUF);
	}

/*
 * leer_mensaje(): Recibe el mensaje y realiza llamadas a las funciones solicitadas
 */
	void leer_mensaje(int cliente_socket, int num) {
		// Leemos el mensaje (Si es vacio, el cliente se ha desconectado)
		memset(buffer, '\0', BUF);                                
		if (!read(cliente_socket, buffer, BUF))
			cerrar(cliente_socket);

		// Descartamos el caso de que el mensaje es vacio
		if (strlen(buffer) > 0) {
			/*
			* REGISTRAR: se inicia el proceso de registro
			*/
			if (!strcmp(buffer, "REGISTRAR\n")) {
					crear_cliente(cliente_socket, num);
			}
			else {
				char *aux = strtok(buffer, " ");

				/*
				* LOGIN <id> <c>: se inicia el proceso de autenticacion
				*/
				if (!strcmp(aux, "LOGIN")) {
					autenticar(cliente_socket, num);
				}
				/*
				* SETNAME <nombre>: Se actualiza el nombre del cliente
				*/
				else if (!strcmp(aux, "SETNAME")) {
					cambiar_nombre(cliente_socket, num);
				}					
			}
		}
	}

/*
 * winner(): Comprueba por columnas, filas y diagonales si el jugador ha ganado.
 * 		     1 - Gana el del turno, 2 - Empate, 0 - No hay ganador
 */
	int winner(int column, int turno) {
		int en_cola;

		//check col
		if (check_col[column] <= rows - SEGUIDOS) {
			en_cola = 1;
			for (int i = check_col[column]; i < check_col[column] + SEGUIDOS; i++) {
				if(tablero[(column * rows) + i] != turno + 1) {
					en_cola = 0;
					break;
				}
			}
			if (en_cola)
				return 1;
		}

		int en_fila = 1;
		//check row
		check_col[column];
		int punto = (column) * rows + check_col[column];
		int i = punto;
		// check los anteriores de la fila
		while (i > check_col[column] & en_fila != SEGUIDOS) {
			i = i - rows;
			if (tablero[i] == turno + 1) {
				en_fila++;
			} else {
				break;
			}
			
		}
		i = punto;
		// check los siguientes de la fila
		while (i < (column + SEGUIDOS - en_fila) * rows + check_col[column] & en_fila != SEGUIDOS) {
			i = i + rows;
			if (tablero[i] == turno + 1) {
				en_fila++;
			} else {
				break;
			}
		}

		if (en_fila == SEGUIDOS)
			return 1;
		
		int en_diag = 1;
		//check diag
		i = punto;
		while (i > i - (rows - 1)) {
			if (tablero[i - (rows - 1)] == turno + 1)
				en_diag++;
			else
				break;
			i = i - (rows - 1);	
		}
		i = punto;
		while (i < i + (rows - 1)) {
			if (tablero[i + (rows - 1)] == turno + 1)
				en_diag++;
			else
				break;
			i = i + (rows - 1);
		}

		if (en_diag == SEGUIDOS)
			return 1;

		//check anti diag
		en_diag = 1;
		i = punto;
		while (i > i - (rows + 1)) {
			if (tablero[i - (rows + 1)] == turno + 1)
				en_diag++;
			else
				break;
			i = i - (rows + 1);	
		}
		i = punto;
		while (i < i + (rows + 1)) {
			if (tablero[i + (rows + 1)] == turno + 1)
				en_diag++;
			else
				break;
			i = i + (rows + 1);
		}

		if (en_diag == SEGUIDOS)
			return 1;

		//check empate
		for (int i = 0; i < cols; i++) {
			if (check_col[i] != 0)
				return 0;
		}
		return 2;
	}

/*
 * win(): Comprueba si ha ganado alguien. Envia mensajes a los jugadores.
 * 		  Devuelve 1 si ha ganado alguien, 0 en otro caso.
 */
	int win(int turno, int column) {
			
		int ganador = winner(column, turno);
		
		// Comprobar si ha ganado alguien o es empate
		if (ganador == 1) {
			// Enviando mensaje de VICTORY y DEFEAT
			printf("Ha ganado %s\n", jugador[turno].nombre);
			write(cliente_socket[turno], "VICTORY\n", 9);
			write(cliente_socket[1 - turno], "DEFEAT\n", 8);
			return 1;
		} else if (ganador == 2) {
			printf("Ha habido un empate\n");
			write(cliente_socket[0], "TIE\n", 5);
			write(cliente_socket[1], "TIE\n", 5);
			return 1;
		}
		return 0;
	}

/*
 * movimiento(): Comprueba si se puede llevarse a cabo el movimiento. 1 - Success
 */
	int movimiento(int column, int turno) {

		if (column > 0 & column <= cols) {
			// Si hay hueco en la columna, el movimiento es correcto
			if (check_col[column - 1] != 0) {
				tablero[(column-1) * rows + (--check_col[column - 1])] = turno;
				return 1;
			}
		}
		
		return 0;
	}

/*
 * dibujar(): Imprime por pantalla el tablero
 */
	void dibujar() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				printf("%d ", tablero[(j * rows) + i]);
				if ((j + 1) % (cols) == 0)
					printf("\n");
			}
		}
	}

/*
 * juego(): Manejador del juego
 */
	void juego() {
		// Elegimos de quien es el primer turno
		int turno = rand() % 2;
		int column;
		int primer = 1;
		int correcto = 0;

		do {
			printf("Turno de %s\n", jugador[turno].nombre);
			// Si es primer turno, enviamos solo URTURN
			if (primer) {
				write(cliente_socket[turno], "URTURN\n", 8);
				primer--;
			}
			// En turnos siguientes se envia URTURN <opponent's movement>
			else {
				memset(buffer, 0, BUF);
				sprintf(buffer, "URTURN %d\n", column);
				write(cliente_socket[turno], buffer, BUF);
			}
				
			do {
				// Lectura del movimiento
				memset(buffer, 0, BUF);

				if (!read(cliente_socket[turno], buffer, BUF)) {
					printf("%s ha salido. %s gana el partido\n", jugador[turno].nombre, jugador[1 - turno].nombre);
					close(cliente_socket[turno]);
					write(cliente_socket[1 - turno], "VICTORY\n", 9);
					close(cliente_socket[1 - turno]);
					kill(pid, SIGKILL);
					exit(0);
				} else {
					strtok(buffer, " ");
					column = atoi(strtok(NULL, " "));

					// Comprobamos si el movimiento es correcto
					correcto = movimiento(column, turno + 1);
					if (correcto) {
						printf("\tColumna %d\n", column);
						write(cliente_socket[turno], "COLUMN OK\n", 11);
						
						// Dibujar el tablero
						//dibujar();
						
					} else {
						printf("\tColumna erronea\n");
						write(cliente_socket[turno], "COLUMN ERROR\n", 14);
					}
				}
			} while (!correcto);
			turno = 1 - turno;
		} while(!win(1 - turno, column - 1));

		// Cerramos el juego
		close(cliente_socket[0]);
		close(cliente_socket[1]);
		kill(pid, SIGKILL);
                terminar(0);
		exit(0);
	}
			
