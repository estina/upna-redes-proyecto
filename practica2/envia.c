/*
 * Practica 2
 * Grupo: rc48
 * Autores:
 * 		Eugeniu Stina
 * 		David Soto
 * Descripcion:
 *		Establece una conexión TCP a <direccion_ip> y ​<puerto> indicados y
 *		envía <mensaje>. Tras esto, cierra la conexión y sale del programa sin
 *		esperar respuesta del datos_conexion.
 *		El <mensaje>​ puede ser una única palabra o varias entre comillas.
 *		La​ <direccion_ip> será del tipo 10.1.1.121, no puede ser un nombre
 *		simbólico.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main (int argc, char *argv[]) {
	// Variables
	int socket_id;
	struct sockaddr_in datos_conexion;

	// Comprobamos argumentos
	if (argc != 4){
		printf("Numero de argumentos incorrecto.\n");
		printf("Formato: envia <direccion_ip> <puerto> <mensaje>\n");
		exit(-1);
	}
	
	// Abrimos el socket
	if ((socket_id = socket(PF_INET, SOCK_STREAM,0)) == -1){
		printf("Error al abrir el socket.\n");
		exit(-1);
	}

	// Rellenamos estructura de la direccion
	datos_conexion.sin_family = AF_INET;
	datos_conexion.sin_port = htons(atoi(argv[2]));
	// inet_aton transforma el string de IP en binario (32 bits)
	inet_aton(argv[1], &datos_conexion.sin_addr);
	
	// Conexion con el servidor mediante protocolo TCP
	if (connect(socket_id, (struct sockaddr*)&datos_conexion, sizeof(datos_conexion)) == -1){
		printf("Error de conexion.\n");
		exit(-1);
	}

	// Conexion establecida, peticion Web
	printf("Conexion OK.\n");	
	char mensaje[50];
	memset(mensaje, 0, 50);
	sprintf(mensaje, "%s\n", argv[3]);
	write(socket_id, mensaje, sizeof(mensaje));
	printf("Enviado: %s",mensaje);

	//Cerrando conexion
	printf("Cerrando conexion.\n");
	close(socket_id);
	exit(0);
}
