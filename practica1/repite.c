#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
 
int main(int argc, char *argv[]) {

	int i;
	char *cadena;
	if (argc == 3) {
	    i = atoi(argv[1]);
		cadena = argv[2];
	} else {
		printf("Falta argumento\n");
		exit(1);
	}

    for (int j = 0; j < i; j++)
		printf("%s\n", cadena);
 
    return 0;
}

