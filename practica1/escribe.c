#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
 
int main(int argc, char *argv[]) {
    int i;
    char *filename;
    FILE *f;
    char buf[256];
 
    if(argc != 3) {
		printf("Faltan argumentos\n");
        exit(1);
    }
    else {
        filename = argv[1];
		i = atoi(argv[2]);
    }
    if (i <= 0) {
        printf("El numero debe ser mayor que 0\n");
        exit(-1);
    }
 
    f = fopen(filename, "w");
    if(f == NULL) {
        printf("No puedo abrir el fichero %s\n", argv[1]);
		exit(1);
    }
 
    memset(buf, 0, 256);
	scanf("%s", buf);

	for (int j = 0; j < i; j++)
		fprintf(f, "%d: %s\n", j+1, buf);
 
 
    fclose(f);
 
    return 0;
}

