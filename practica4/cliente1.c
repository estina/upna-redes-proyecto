/*
 * Practica 4: cliente1.c
 * Grupo: rc48
 * Autores:
 * 		Eugeniu Stina
 * 		David Soto
 * Descripcion:
 *		Este cliente establece conexion TCP con un servidor e incia un registro.
 */
 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//  Constantes
#define BUF 2000                                //  Tamaño maximo por cada lectura/escritura en el socket  
#define NombreFichero "DatosCliente.txt"        //  Nombre del fichero donde se guardaran los datos del cliente
#define MAX_TAM_ID 12                           //  Tamaño máximo del id
#define TAM_MAX_NAME 16                         //  Tamaño máximo del nombre

//  Funciones
/*
 *  Intenta establecer conexion con el cliente, si esta registrado hara log in y si no se registrara
 */
void iniciarSesion();
/*
 *  Cierra la conexion con el socket e imprime el error err por pantalla
 */
void cerrar(char* err);
/*
 *  Lee un comando del socket y ejecuta la funcion tratarMensaje
 */
void tratarRespuesta();
/*
 *  Ejecuta las instrucciones necesarias dependiendo del comando de entrada
 */
void tratarMensaje(char *in);
/*
 *  Busca el fichero de datos del cliente y lee el id y el c
 */
int leerDatos();
/*
 *  Guarda los datos del cliente en el fichero
 */
int guardarDatos();
/*
 *  Pregunta al cliente que opcion quiere hacer y envia el comando correspondiente al servidor
 */
int menu();

//  Variables globales
int sock,c;
char id[MAX_TAM_ID];
//  Main
int main(int argc, char *argv[]){
        //Variables	
	int err;
	struct sockaddr_in servidor;
        char buf[BUF];
        
	//Comprobamos argumentos
	if (argc != 3){
		printf("Numero de argumentos incorrecto.\n");
		printf("Formato: cliente1 <direccion_ip> <puerto>\n");
		exit(-1);
	}
	
	//Abrimos el socket
	sock = socket(PF_INET, SOCK_STREAM,0);
	if (sock == -1){
		printf("Error al abrir el socket.\n");
		exit(-1);
	}
	//Rellenamos estructura de la direccion
	servidor.sin_family = AF_INET;
	servidor.sin_port=htons(atoi(argv[2]));
	//inet_aton(direccion_ip_tipo_"127.0.0.1",puntero_a_estructura_en_binario);
	inet_aton(argv[1], &servidor.sin_addr);
	

	//Conexion con el servidor TCP
	err = connect(sock,(struct sockaddr*)&servidor,sizeof(servidor));
	if (err == -1){
		printf("Error de conexion.\n");
		exit(-1);
	}

	//Conexion establecida, comprobamos registro de usuario	
	iniciarSesion();
	
        //  Bucle
	while (menu() != -1){
            tratarRespuesta();
        }
        printf("Cerrando sesion.\n");
        cerrar(NULL);
}
//
void tratarRespuesta(){
    char *aux;
    char in[BUF];    
    
    memset(in,'\0',BUF);    
    if (read(sock,in,BUF) == -1){
        cerrar("Error en la lectura del socket");
    }    
    aux = strtok(in,"\n");
    while (aux != NULL){
        tratarMensaje(aux);
        aux = strtok(NULL,"\n");
    }
}
//
void tratarMensaje(char *in){
    char *aux;
    char out[BUF];
    
    memset(out,'\0',BUF);
    aux = strtok(in," ");
    if (strcmp(aux,"RESUELVE") == 0){
        int a = atoi(strtok(NULL," "));
        int b = atoi(strtok(NULL," "));
        c = a + b;
        sprintf(out,"RESPUESTA %d\n",c);
        if (write(sock,out,BUF) == -1){
            cerrar("Error al enviar datos al servidor");
        }
        tratarRespuesta();
    } else if (strcmp(aux,"REGISTRADO") == 0) {
        if (strcmp(strtok(NULL," "),"OK") == 0){
            sprintf(id,"%s",strtok(NULL," "));
            printf("ID: %s.\n",id);
            printf("C: %d.\n",c);
            if (guardarDatos() == -1){
                printf("Se ha registrado en el servidor pero no se han podido guardar los datos.\n");
            } else {
                printf("Se ha registrado correctamente.\n");
            }
        } else {
            cerrar("Error al registrarse en el server");
        }
    } else if (strcmp(aux,"LOGIN") == 0) {
        if (strcmp(strtok(NULL," "),"OK\n") == 0){
            printf("Conectado al servidor.\n");
        } else {                        
            cerrar("Error al conectarse al servidor.");
        }
    } else if (strcmp(aux,"SETNAME") == 0) {
        if (strcmp(strtok(NULL," "),"OK") == 0){
            printf("Nombre cambiado satisfactoriamente.\n");
        } else {
            printf("Error al cambiar el nombre.\n");
        }
    } else if (strcmp(aux,"GETNAME") == 0) {
        printf("Nombre actual: %s.\n",strtok(NULL," "));
    } else if (strcmp(aux,"LISTADO") == 0) {
        printf("Listado de usuarios conectados: %s.\n",strtok(NULL," "));
    } else if (strcmp(aux,"UPTIME") == 0) {
        int t1 = atoi(strtok(NULL," "));
        int t2 = atoi(strtok(NULL," "));
        printf("Tiempo que lleva el servidor funcionando: %d.\n",t1);
        printf("Tiempo que llevas conectado: %d.\n",t2);
    }        
}
//
void cerrar(char* err){
    if (err != NULL){
        printf("Error: %s.\n",err);
    }
    close(sock);
    exit(0);
}
//
void iniciarSesion(){
    char aux[BUF];
    memset(aux,'\0',BUF);
    if (leerDatos() == -1){             //  Registro
        sprintf(aux,"REGISTRAR\n");                
    } else {                            //  Login
        sprintf(aux,"LOGIN %d %s\n",c,id);                
    }
    printf("Mandando: %s.\n",aux);
    write(sock,aux,BUF);
    tratarRespuesta();    
}
//
int guardarDatos(){
    FILE *datos;
    
    datos = fopen(NombreFichero, "w+");
    if (datos == NULL){
        return -1;
    }
    fprintf(datos,"%s\n",id);
    fprintf(datos,"%d\n",c);    
    fclose(datos);
    return 0;
}

//
int leerDatos(){
    FILE *datos;
    
    datos = fopen(NombreFichero, "r");
    if (datos == NULL){
        return -1;
    }
    fscanf(datos,"%s",id);
    fscanf(datos,"%d",&c);
    fclose(datos);
    return 0;
}
// 
int menu(){
    int r;
    char buf[BUF];
    memset(buf,'\0',BUF);
    do {
        printf("Elige una opción.\n");
        printf("1 -- Cambiar nombre.\n");
        printf("2 -- Consultar nombre.\n");
        printf("3 -- Listado de clientes conectados.\n");
        printf("4 -- Tiempo servicio.\n");
        printf("0 -- Salir.\n");        
        printf("-->: ");
        scanf("%d",&r);
    } while (r < 0 || r > 4);
    switch(r){
        case 0:
            return -1;            
        case 1:
            printf("Dime tu nuevo nombre: ");
            char nombre[TAM_MAX_NAME];
            scanf("%s",nombre);
            sprintf(buf,"SETNAME %s\n",nombre);
            break;
        case 2:
            sprintf(buf,"GETNAME\n");            
            break;
        case 3:
            sprintf(buf,"LISTA\n");
            break;
        case 4:
            sprintf(buf,"UPTIME\n");
            break;                    
    }
    return write(sock,buf,BUF);    
}


