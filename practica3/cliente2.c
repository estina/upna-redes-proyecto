/*
 * Practica 3: cliente2.c
 * Grupo: rc48
 * Autores:
 * 		Eugeniu Stina
 * 		David Soto
 * Descripcion:
 *		El programa consiste en una conexion cliente TCP con un servidor en la que
 *              trata de registrarse.
 *              El cliente guardara el identificador dado en un fichero DatosCliente.txt
 *              PETICIONES:
 *              CLIENTE                     SERVIDOR
 *              ------------------REGISTRO----------------     
 *              REGISTRAR       --->        
 *		                <---        RESUELVE <a> <b>
 *              RESPUESTA <c>   --->    
 *                              <---        REGISTRADO OK <id>  (si c = a + b)
 *                              <---        REGISTRADO ERROR    (si no se resuelve la peticion)
 *              ------------------LOGIN----------------
 *              LOGIN <id> <c>  --->
 *                              <---        LOGIN OK            (si se ha registrado en el server)
 *                              <---        LOGIN ERROR         (si no esta registrado)
 * 
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//  Constantes
#define BUF 2000
#define NombreFichero "DatosCliente.txt"
#define MAX_TAM_ID 12

//  Funciones del programa
void cerrar(char* err, int sock);
int estaRegistrado();
int registrarUsuario(int sock);
int logUsuario(int sock);

//  Main
int main (int argc, char *argv[]) {
	//Variables
	int sock;
	int err;
	struct sockaddr_in servidor;

        
	//Comprobamos argumentos
	if (argc != 3){
		printf("Numero de argumentos incorrecto.\n");
		printf("Formato: cliente1 <direccion_ip> <puerto>\n");
		exit(-1);
	}
	
	//Abrimos el socket
	sock = socket(PF_INET, SOCK_STREAM,0);
	if (sock == -1){
		printf("Error al abrir el socket.\n");
		exit(-1);
	}
	//Rellenamos estructura de la direccion
	servidor.sin_family = AF_INET;
	servidor.sin_port=htons(atoi(argv[2]));
	//inet_aton(direccion_ip_tipo_"127.0.0.1",puntero_a_estructura_en_binario);
	inet_aton(argv[1], &servidor.sin_addr);
	

	//Conexion con el servidor TCP
	err = connect(sock,(struct sockaddr*)&servidor,sizeof(servidor));
	if (err == -1){
		printf("Error de conexion.\n");
		exit(-1);
	}

	//Conexion establecida, peticion Web
	if (estaRegistrado() == -1){
            //Registrar
            registrarUsuario(sock);
        } else {
            //Login
            logUsuario(sock);
        }
        //Usuario en linea, de momento mantenemos conexion
        printf("Pulse cualquier tecla para continuar.\n");
        getchar();        
        
	//Cerrar conexión satisfactoriamente
        printf("Cerrando conexion.\n");
	cerrar(NULL,sock);		
}

//  Implementacion de las funcioes

//  cerrar: si hay un mensaje de error lo imprime por pantalla.
//          cierra el socket y sale
void cerrar(char* err, int sock){
    if (err != NULL){
        printf("Error: %s.\n",err);
    }
    close(sock);
    exit(0);
}

//  registrarUsuario: intercambia mensajes con el servidor para registrar
//                    al usuario. Devuelve 0 si se ha registrado correctamente
//                    o -1 si ha ocurrido algun error.
int registrarUsuario(int sock){
    FILE* datos;
    int a,b,c;
    char buf[BUF];
    char *aux,*id;
    //  Protocolo de registro
    write(sock,"REGISTRAR\n",strlen("REGISTRAR\n"));
    
    read(sock,buf,BUF);
    aux = buf;
    //  Comprobamos respuesta
    if (strcmp(strtok(aux," "), "RESUELVE")== 0){
        //  Leemos los argumentos a resolver
        a = atoi(strtok(NULL," "));
        b = atoi(strtok(NULL," "));          
        c = a + b;
        memset(buf,'\0',BUF);
        sprintf(buf,"RESPUESTA %d\n",c);
        //  Enviamos respuesta
        write(sock,buf,BUF);
        
        //  Leemos y comprobamos si el registro ha sido efectivo
        read(sock,buf,BUF);
        aux = buf;
        if (strcmp(strtok(aux," "),"REGISTRADO") == 0){
            aux = strtok(NULL, " ");
            if (strcmp(aux, "OK") == 0){
                //  Registro finalizado, leemos nuestro id y lo almacenamos
                id = strtok(NULL,"\n");
                printf("Usuario registrado con id: %s y c: %d.\n",id,c);
                datos = fopen(NombreFichero,"w+");
                if (datos == NULL){
                    cerrar("No se puede abrir el fichero para guardar los datos",sock);
                }
                fprintf(datos,"%s\n%d",id,c);                
                fclose(datos);
            } else {
                cerrar("Registro ERROR",sock);
            }            
        } else {
            cerrar("Respuesta del servidor esperada (REGISTRADO)",sock);
        }        
    } else {
        cerrar("Respuesta del servidor esperada (RESUELVE)",sock);
    }
}


//  estaRegistrado: comprueba si el fichero de datos esta creado. Si es así devuelve
//                  0, en otro caso devuelve -1
int estaRegistrado(){  
    FILE *datos;
    datos = fopen(NombreFichero,"r");
    if (datos == NULL){
        return -1;
    }
    fclose(datos);
    return 0;
}

//  logUsuario: abre el fichero para leer los datos del usuario e intenta logearse en
//              el servidor.
int logUsuario(int sock){
    FILE *datos;
    char buf[BUF], id[MAX_TAM_ID];
    char *aux;
    int c;
    
    
    datos = fopen(NombreFichero,"r");
    fscanf(datos,"%[^\n]",id);
    fscanf(datos,"%d",&c);
    fclose(datos);
    //  Protocolo de LOGIN
    printf("Datos del usuario: %s %d.\n",id,c);
    memset(buf,'\0',BUF);
    sprintf(buf,"LOGIN %s %d\n",id,c);
    write(sock,buf,BUF);
    memset(buf,'\0',BUF);
    read(sock,buf,BUF);
    printf("Recibido login: %s",buf);
    
    //  Comprobamos la respuesta
    aux = buf;
    if (strcmp(strtok(aux," "),"LOGIN") == 0){
        aux = strtok(NULL, " ");
        if (strcmp(aux, "OK\n") == 0){
            printf("Logeado en el server.\n");
        } else {
            cerrar("Login ERROR",sock);
        }
    } else {
        cerrar("Respuesta del servidor esperada (LOGIN)",sock);
    }    
}

