/*
 * Practica 3: cliente1.c
 * Grupo: rc48
 * Autores:
 * 		Eugeniu Stina
 * 		David Soto
 * Descripcion:
 *		El programa consiste en una conexion cliente TCP con un servidor en la que
 *              trata de registrarse.
 *              El cliente guardara el identificador dado en un fichero DatosCliente.txt
 *              PETICIONES:
 *              CLIENTE                     SERVIDOR
 *              REGISTRAR       --->        
 *		                <---        RESUELVE <a> <b>
 *              RESPUESTA <c>   --->    
 *                              <---        REGISTRADO OK <id>  (si c = a + b)
 *                              <---        REGISTRADO ERROR    (si no se resuelve la peticion)
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define BUF 2000
#define NombreFichero "DatosCliente.txt"
int main (int argc, char *argv[]) {
	//Variables
	int sock;
	int err;
	struct sockaddr_in servidor;
	char buf[BUF]; 
        char *aux;
        FILE* datos;

	//Comprobamos argumentos
	if (argc != 3){
		printf("Numero de argumentos incorrecto.\n");
		printf("Formato: cliente1 <direccion_ip> <puerto>\n");
		exit(-1);
	}
	
	//Abrimos el socket
	sock = socket(PF_INET, SOCK_STREAM,0);
	if (sock == -1){
		printf("Error al abrir el socket.\n");
		exit(-1);
	}
	//Rellenamos estructura de la direccion
	servidor.sin_family = AF_INET;
	servidor.sin_port=htons(atoi(argv[2]));
	//inet_aton(direccion_ip_tipo_"127.0.0.1",puntero_a_estructura_en_binario);
	inet_aton(argv[1], &servidor.sin_addr);
	

	//Conexion con el servidor TCP
	err = connect(sock,(struct sockaddr*)&servidor,sizeof(servidor));
	if (err == -1){
		printf("Error de conexion.\n");
		exit(-1);
	}

	//Conexion establecida, peticion Web
	
        //Mandamos peticion de REGISTRAR
        write(sock,"REGISTRAR\n",strlen("REGISTRAR\n"));
        printf("Enviada peticion de registro.\n");
        
        //Leemos la respuesta del servidor
        read(sock,buf,BUF);          
        
        //Comprobamos la orden recibida
        aux = buf;
        if (strcmp(strtok(aux," "), "RESUELVE")== 0){
            int a = atoi(strtok(NULL," "));
            int b = atoi(strtok(NULL," "));            
            
            //Enviamos  la respuesta al servidor
            memset(buf,'\0',BUF);
            sprintf(buf,"RESPUESTA %d\n",a + b);
            write(sock,buf,BUF);               
            
            //Leemos del servidor para comprobar si nos hemos registrado con exito
            read(sock,buf,BUF);
            aux = buf;            
            if (strcmp(strtok(aux," "),"REGISTRADO") == 0){
                aux = strtok(NULL, " ");
                if (strcmp(aux, "OK") == 0){
                    //Registro satisfactorio
                    //Leemos el idçentificador
                    char* id;
                    id = strtok(NULL,"\n");
                    printf("Resgistro ok. Identificador: %s. C: %d.\n",id,a + b); 
                    //Guardamos en el fichero
                    datos = fopen(NombreFichero,"w+");
                    fprintf(datos,"%s\n",id);
                    printf("Datos guardados en %s\n",NombreFichero);
                    fclose(datos);
                } else {
                    //Error en el registro
                    printf("Error al registrarse.\n");                    
                }
            }else{
                //Error al recibir REGISTRADO OK <id>
            }
        }else{
            //Error al recibir RESUELVE <a> <b>
        }
                
	//Cerrando conexion
	printf("Cerrando conexion.\n");
	close(sock);
	exit(0);
}



