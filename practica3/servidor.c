/*
 * Practica 3: SERVIDOR.C
 * Grupo: rc48
 * Autores:
 * 		Eugeniu Stina
 * 		David Soto
 * Descripcion:
 *		Este servidor reconoce siguientes comandos:
 * 			REGISTRAR			REGISTRA AL CLIENTE
 * 			LOGIN <id> <c>		AUTENTIFICA AL CLIENTE
 * 			GETNAME 			ENVIA EL NOMBRE DEL CLIENTE
 * 			SETNAME <nombre> 	CAMBIA EL NOMBRE DEL CLIENTE
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// Constantes necesarias
#define BUF 50 // El tamano del buffer
#define MAX_REGISTRADOS 20 // El numero maximo de clientes registrados
#define MIN_ID 6 // El tamano minimo del id
#define MAX_ID 12 // El tamano maximo del id
#define TAM_NOMBRE 16 // El tamano maximo del nombre del cliente
#define FICHERO "clientes.txt"

// Estructura del cliente
	typedef struct cliente_struct{
		char id[MAX_ID];
		int codigo;
		char nombre[TAM_NOMBRE + 1]; 
	} cliente_struct;

// Variables globales
	char buffer[BUF];
	int registrados = 0;
	int server_socket;
	int cliente_socket;
	cliente_struct lista_clientes[MAX_REGISTRADOS]; // Lista de clientes registrados (NO CONECTADOS!)
	cliente_struct conectado; 			// Estructura del cliente conectado

// Funciones necesarias
	void leerclientes();
	void crear_cliente();
	void cerrar();
	void autenticar();
	void cambiar_nombre();
	void enviar_nombre();
	
// Terminando correctamente el servidor
	void terminar(int x) {
		// Cerrando conexion
		printf("\nCerrando conexion.\n");
		close(server_socket);
		exit(0);
	}

int main (int argc, char *argv[]) {
	// Variables
	struct sockaddr_in servidor, cliente;
	int len = sizeof(cliente);

	// Comprobamos argumentos
	if (argc != 2){
		printf("Numero de argumentos incorrecto.\n");
		printf("Formato: servidor <puerto>\n");
		exit(-1);
	}
	
	// Abrimos el socket
	if ((server_socket = socket(PF_INET, SOCK_STREAM,0)) == -1){
		printf("Error al abrir el socket.\n");
		exit(-1);
	}

	// Redirigimos la señal SIGINT: Ctrl + C para cerrar correctamente los socket
	struct sigaction stop;
	stop.sa_handler = &terminar;
	stop.sa_flags = 0;
	sigemptyset(&stop.sa_mask);
	sigaction(SIGINT, &stop, NULL); 

	// Rellenamos estructura del servidor (la IP no importa)
	servidor.sin_family = AF_INET;
	servidor.sin_port = htons(atoi(argv[1]));
	servidor.sin_addr.s_addr = INADDR_ANY;
	
	// Reservamos el puerto
	if (bind(server_socket, (struct sockaddr*)&servidor, sizeof(servidor)) == -1){
		printf("Error al reservar el puerto.\n");
		exit(-1);
	}

	printf("Escuchando conexiones en el puerto\n");

	// Esperando a los clientes
	listen(server_socket, 5);

	while (1) {	

		// Se crea un socket para el cliente entrado
		cliente_socket = accept(server_socket, (struct sockaddr*)&cliente, &len);

		// Creamos un hijo por cada cliente
		int pid = fork();
		
		// Proceso hijo se crea por cada cliente conectado.
		if (pid == 0) {

			printf("[%d] Conexion establecida desde la IP %s puerto %d\n",getpid() , inet_ntoa(cliente.sin_addr), htons(cliente.sin_port));

			// Leemos la información de los clientes ya guardados
			leerclientes();
			
			// Establecemos el seed del random
			srand(time(NULL));

			strcpy(conectado.id, "-1");

			while (1) {
				// Leemos el mensaje (Si es vacio, el cliente se ha desconectado)
				memset(buffer, '\0', BUF);                                
				if (!read(cliente_socket, buffer, BUF))
					cerrar();

				// Descartamos el caso de que el mensaje es vacio
				if (strlen(buffer) > 0) {
					/*
					* REGISTRAR: se inicia el proceso de registro
					*/
					if (!strcmp(buffer, "REGISTRAR\n")) {
						if (strcmp(conectado.id, "-1") != 0)
							cerrar();
						else
							crear_cliente();
					}
					/*
					* GETNAME: se envia el nombre del cliente
					*/
					else if (!strcmp(buffer, "GETNAME\n")) {
						if (!strcmp(conectado.id, "-1"))
							cerrar();
						else
							enviar_nombre();
					}
					else {
						char *aux = strtok(buffer, " ");

						/*
						* LOGIN <id> <c>: se inicia el proceso de autenticacion
						*/
						if (!strcmp(aux, "LOGIN")) {
							if(strcmp(conectado.id, "-1") != 0)
								cerrar();
							else
								autenticar();
						}
						/*
						* SETNAME <nombre>: Se actualiza el nombre del cliente
						*/
						else if (!strcmp(aux, "SETNAME")) {
							if (!strcmp(conectado.id, "-1"))
								cerrar();
							else
								cambiar_nombre();
						}					
					}
				}
			}
		}
	}
}

/*
 * leerclientes(): Almacena en la estructura lista_clientes los datos almacenados en el archivo FICHERO
 * 		Necesario para saber los identificadores de todos los clientes para generar nuevos IDs unicos.
 */
	void leerclientes() {
		FILE *fichero;
		int i = 0;
		fichero = fopen(FICHERO, "a+");
		while (fgets(buffer, sizeof(buffer), fichero)) {
			strcpy(lista_clientes[i].id, strtok(buffer, "\t"));
			lista_clientes[i].codigo = atoi(strtok(NULL, "\t"));
			strcpy(lista_clientes[i].nombre, strtok(NULL, "\n"));
			i++;
			memset(buffer, 0, BUF);
		}
		registrados = i;
		fclose(fichero);
	}

/*
 * cerrar(): Se cierra la conexion con el cliente y finaliza el proceso hijo
 */
	void cerrar() {
		printf("[%d] Conexion cerrada\n", getpid());
		shutdown(cliente_socket, 2);
		exit(0);
	}

/*
 * guardar_clientes(): Cuando se registra un cliente nuevo, todos los datos de todos los clientes se
 * 		almacenan en el archivo FICHERO
 */
	void guardar_clientes() {
		FILE *fichero;
		fichero = fopen(FICHERO,"w");
		for (int i = 0; i < registrados; i++) {
			fprintf(fichero, "%s\t%d\t%s\n", lista_clientes[i].id, lista_clientes[i].codigo, lista_clientes[i].nombre);
		}
		fclose(fichero);
	}

/*
 * unico(): Compara el identificador generado con todos los identificadores almacenados para que no haya
 * 		dos identificadores identicos.
 */
	int unico(char *identidad) {
		for (int i = 0; i < registrados; i++) {
			if (strcmp(lista_clientes[i].id, identidad) == 0)
				return 0;
		}
		return 1;
	}

/*
 * enviar_id(): Genera un identificador comprobando que es unico y lo envia al cliente.
 */
	void enviar_id() {
		
		// Generando longitud de ID y otras variables
		int longitud = rand() % MIN_ID + (MAX_ID - MIN_ID);
		char *identidad = calloc(longitud, longitud);

		// Generar ID hasta que no sea unico
		do {
			// Generando el ID de letras minusculas y numeros
			for (int i = 0; i < longitud; i++) {
				if (rand() % 2 == 0)
					identidad[i] = rand() % 10 + 48;
				else
					identidad[i] = rand() % 25 + 97;
			}
		} while (!unico(identidad));
		

		// Enviando mensaje al cliente
		memset(buffer, 0, BUF);
		sprintf(buffer, "REGISTRADO OK %s\n", identidad);
		write(cliente_socket, buffer, BUF);

		printf("[%d] Asignando id %s.\n", getpid(), identidad);

		// Almacena la información de un nuevo cliente
		strcpy(lista_clientes[registrados].id, identidad);
		strcpy(lista_clientes[registrados++].nombre, "Invitado");
	}

/*
 * registrar(): Envia mensajes de comprobacion, solicita generacion del identificador
 * 		y almacena todo en la estructura client. Al finalizar el registro, solicita 
 * 		que se almacene todos los datos en el fichero FICHERO
 */
	int registrar() {
		
		printf("[%d] Recibida petición de registro.\n", getpid());	

		// Generando dos enteros aleatorios de 0 hasta 99
		int a = rand() % 100;
		int b = rand() % 100;

		printf("[%d] Estableciendo prueba %d + %d.", getpid(), a, b);

		// Enviamos RESUELVE <a> <b>
		memset(buffer, 0, BUF);
		sprintf(buffer, "RESUELVE %d %d\n", a, b);
		write(cliente_socket, buffer, BUF);

		// RESPUESTA <c>
		memset(buffer, 0, BUF);
		read(cliente_socket, buffer, BUF);

		// Dividimos la cadena en RESPUESTA y <c>
		char *aux = strtok(buffer, " ");

		// Comprobamos si es el mensaje esperado
		if (strcmp(aux, "RESPUESTA") == 0) {
			
			// Comprobamos el valor de <c>
			int c = atoi(strtok(NULL, " "));
			
			printf(" Recibido %d, ", c); 

			// Si la suma es correcta, registramos al cliente
			if (c == a + b) {
				printf("prueba superada.\n");

				// Guardamos el numero secreto en la estructura del cliente
				lista_clientes[registrados].codigo = c;
				
				// Enviamos REGISTRADO OK <id>
				enviar_id();

				// Guardamos los datos del cliente para futuras modificaciones
				conectado = lista_clientes[registrados - 1];

				// Guardamos al cliente en el fichero junto con los demas clientes leidos
				guardar_clientes();
				
				return 1;
				
			}
			// En caso contrario, devolvemos mensaje de error y cerramos la conexion con el cliente
			else {
				printf("prueba NO superada\n");
				memset(buffer, 0, BUF);
				sprintf(buffer, "REGISTRADO ERROR\n");
				write(cliente_socket, buffer, BUF);
			}
		} 
		else // En caso de que la conexion se ha interrumpido
			printf("[%d] Error de conexion\n", getpid());
		
		return 0;
	}

/*
 * crear_cliente(): Manejador del proceso REGISTRA
 */
	void crear_cliente() {

			// Si ha llegado al limite, no permite mas registros
			if (registrados >= MAX_REGISTRADOS) {
				printf("Alcanzado el numero maximo de clientes registrados\n");
				cerrar();
			}
				
			// Iniciar el proceso de registro
			if (!registrar(cliente_socket)) {
				cerrar();
			}
	}

/*
 * autenticar(): Hace LOGIN con el cliente
 */
	void autenticar() {
		char *id = strtok(NULL, " ");
		for (int i = 0; i < registrados; i++) {
			if (!strcmp(lista_clientes[i].id, id)) {
				if (atoi(strtok(NULL, " ")) == lista_clientes[i].codigo) {
					conectado = lista_clientes[i];
					printf("[%d] Usuario con id %s aceptado\n", getpid(), conectado.id);
					memset(buffer, 0, BUF);
					sprintf(buffer, "LOGIN OK\n");
					write(cliente_socket, buffer, BUF);
					return;
				}
			}
		}
		memset(buffer, 0, BUF);
		sprintf(buffer, "LOGIN ERROR\n");
		write(cliente_socket, buffer, BUF);
		cerrar();
	}

/*
 * cambiar_nombre(): Actualiza la lista de clientes registrados, cambia el nombre del cliente y guarda la lista
 */
	void cambiar_nombre() {
		// Guardamos el nombre recibido
		char *name = calloc(TAM_NOMBRE, 1);
		strcpy(name, strtok(NULL, " "));                
		// Necesario para eliminar el \n en el nombre
		int len = strlen(name);
		if (len > 0 && name[len-1] == '\n')
			name[--len] = '\0';

		// Eliminamos los elementos sobrantes (El nombre = 16 letras)
		if (len > TAM_NOMBRE) {
			for (int i = len; i >= TAM_NOMBRE; i--)
				name[i] = '\0'; 
		}

		// Leemos lista de clientes desde el fichero
		leerclientes();

		// Buscamos a nuestro cliente dentro de la lista
		for (int i = 0; i < registrados; i++) {
			if (!strcmp(conectado.id, lista_clientes[i].id)) {
				strcpy(lista_clientes[i].nombre, name);
				strcpy(conectado.nombre, name);
				printf("[%d] Usuario con id %s ha cambiado su nombre a %s\n", getpid(), conectado.id, conectado.nombre);
				memset(buffer, 0, BUF);
				sprintf(buffer, "SETNAME OK\n");
				write(cliente_socket, buffer, BUF);
				// Guardamos nuestra lista actualizada
				guardar_clientes();
				return;
			}
		}
		// Cerrar la conexion o no???
		memset(buffer, 0, BUF);
		sprintf(buffer, "SETNAME ERROR\n");
		write(cliente_socket, buffer, BUF);
		//cerrar();
	}

/*
 * enviar_nombre(): Envia el nombre del cliente al mismo
 */
	void enviar_nombre() {
		memset(buffer, 0, BUF);
		printf("[%d] Usuario con id %s ha solicitado su nombre\n", getpid(), conectado.id);
		sprintf(buffer, "GETNAME %s\n", conectado.nombre);
		write(cliente_socket, buffer, BUF);
	}
