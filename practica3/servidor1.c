/*
 * Practica 3: SERVIDOR1.C
 * Grupo: rc48
 * Autores:
 * 		Eugeniu Stina
 * 		David Soto
 * Descripcion:
 *		El sistema de registro se basa en una sencilla prueba que debe completar
 *		el cliente con los datos facilitados por el servidor.   
 * 		El servidor responderá con un identificador único para él.
 *		El servidor llevará un control de todos los identificadores que ha dado 
 *		para no repetir el mismo a dos usuarios distintos.
 *
 *      El servidor guardara el identificador junto con numero secreto y nombre en id_clientes.txt
 *              PETICIONES:
 *              CLIENTE                     SERVIDOR
 *              REGISTRAR       --->        
 *		                <---        RESUELVE <a> <b>
 *              RESPUESTA <c>   --->    
 *                              <---        REGISTRADO OK <id>  (si c = a + b)
 *                              <---        REGISTRADO ERROR    (si no se resuelve la peticion)		
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

// Constantes necesarias
#define BUF 50 // El tamano del buffer
#define MAX 20 // El numero maximo de clientes registrados

// Estructura del cliente
	typedef struct cliente_struct{
		char id[12];
		int codigo;
		char nombre[16]; 
	} cliente_struct;

// Variables globales
	char buffer[BUF];
	int registrados = 0;
	int server_socket;
	int cliente_socket;
	cliente_struct lista_clientes[MAX]; // Lista de clientes registrados (NO CONECTADOS!)

// Funciones necesarias
	void leerclientes();
	void crear_cliente();
	void cerrar();
	
// Terminando correctamente el servidor
	void terminar(int x) {
		// Cerrando conexion
		printf("\nCerrando conexion.\n");
		close(server_socket);
		exit(0);
	}

int main (int argc, char *argv[]) {
	// Variables
	struct sockaddr_in servidor, cliente;
	int len = sizeof(cliente);

	// Comprobamos argumentos
	if (argc != 2){
		printf("Numero de argumentos incorrecto.\n");
		printf("Formato: servidor <puerto>\n");
		exit(-1);
	}
	
	// Abrimos el socket
	if ((server_socket = socket(PF_INET, SOCK_STREAM,0)) == -1){
		printf("Error al abrir el socket.\n");
		exit(-1);
	}

	// Redirigimos la señal SIGINT: Ctrl + C para cerrar correctamente los socket
	struct sigaction stop;
	stop.sa_handler = &terminar;
	stop.sa_flags = 0;
	sigemptyset(&stop.sa_mask);
	sigaction(SIGINT, &stop, NULL); 

	// Rellenamos estructura del servidor (la IP no importa)
	servidor.sin_family = AF_INET;
	servidor.sin_port = htons(atoi(argv[1]));
	servidor.sin_addr.s_addr = INADDR_ANY;
	
	// Reservamos el puerto
	if (bind(server_socket, (struct sockaddr*)&servidor, sizeof(servidor)) == -1){
		printf("Error al reservar el puerto.\n");
		exit(-1);
	}

	printf("Escuchando conexiones en el puerto\n");

	// Esperando a los clientes
	listen(server_socket, 5);

	while (1) {	

		// Se crea un socket para el cliente entrado
		cliente_socket = accept(server_socket, (struct sockaddr*)&cliente, &len);

		// Creamos un hijo por cada cliente
		int pid = fork();
		
		// Proceso hijo se crea por cada cliente conectado.
		if (pid == 0) {

			printf("[%d] Conexion establecida desde la IP %s puerto %d\n",getpid() , inet_ntoa(cliente.sin_addr), htons(cliente.sin_port));

			// Leemos la información de los clientes ya guardados
			leerclientes();
			
			// Establecemos el seed del random
			srand(time(NULL));

			while (1) {
				// Leemos el mensaje (Si es vacio, el cliente se ha desconectado)
				memset(buffer, 0, BUF);
				if (!read(cliente_socket, buffer, BUF)) {
					cerrar();
				}

				/*
				 * REGISTRAR: se inicia el proceso de registro
				 */
				if (!strcmp(buffer, "REGISTRAR\n")) {
					crear_cliente();
				}
				/*
				 * EXIT: el cliente se desconecta
				 */
				else if (!strcmp(buffer, "EXIT\n")) {
					cerrar();
				} else
					cerrar();				
			}
		}
	}
}

/*
 * leerclientes(): Almacena en la estructura lista_clientes los datos almacenados en el archivo id_clientes.txt
 * 		Necesario para saber los identificadores de todos los clientes para generar nuevos IDs unicos.
 */
	void leerclientes() {
		FILE *fichero;
		int i = 0;
		fichero = fopen("id_clientes.txt", "a+");
		while (fgets(buffer, sizeof(buffer), fichero)) {
			strcpy(lista_clientes[i].id, strtok(buffer, "\t"));
			lista_clientes[i].codigo = atoi(strtok(NULL, "\t"));
			strcpy(lista_clientes[i].nombre, strtok(NULL, "\n"));
			i++;
			memset(buffer, 0, BUF);
		}
		registrados = i;
		fclose(fichero);
	}

/*
 * cerrar(): Se cierra la conexion con el cliente y finaliza el proceso hijo
 */
	void cerrar() {
		printf("[%d] Conexion cerrada\n", getpid());
		shutdown(cliente_socket, 2);
		exit(0);
	}

/*
 * guardar_clientes(): Cuando se registra un cliente nuevo, todos los datos de todos los clientes se
 * 		almacenan en el archivo id_clientes.txt
 */
	void guardar_clientes() {
		FILE *fichero;
		fichero = fopen("id_clientes.txt","w");
		for (int i = 0; i < registrados; i++) {
			fprintf(fichero, "%s\t%d\t%s\n", lista_clientes[i].id, lista_clientes[i].codigo, lista_clientes[i].nombre);
		}
		fclose(fichero);
	}

/*
 * unico(): Compara el identificador generado con todos los identificadores almacenados para que no haya
 * 		dos identificadores identicos.
 */
	int unico(char *identidad) {
		for (int i = 0; i < registrados; i++) {
			if (strcmp(lista_clientes[i].id, identidad) == 0)
				return 0;
		}
		return 1;
	}

/*
 * enviar_id(): Genera un identificador comprobando que es unico y lo envia al cliente.
 */
	void enviar_id() {
		
		// Generando longitud de ID y otras variables
		int longitud = rand() % 6 + 6;
		char *identidad = calloc(longitud, longitud);

		// Generar ID hasta que no sea unico
		do {
			// Generando el ID de letras minusculas y numeros
			for (int i = 0; i < longitud; i++) {
				if (rand() % 2 == 0)
					identidad[i] = rand() % 10 + 48;
				else
					identidad[i] = rand() % 25 + 97;
			}
		} while (!unico(identidad));
		

		// Enviando mensaje al cliente
		memset(buffer, 0, BUF);
		sprintf(buffer, "REGISTRADO OK %s\n", identidad);
		write(cliente_socket, buffer, BUF);

		printf("[%d] Asignando id %s.\n", getpid(), identidad);

		// Almacena la información de un nuevo cliente
		strcpy(lista_clientes[registrados].id, identidad);
		strcpy(lista_clientes[registrados++].nombre, "Invitado");
	}

/*
 * registrar(): Envia mensajes de comprobacion, solicita generacion del identificador
 * 		y almacena todo en la estructura client. Al finalizar el registro, solicita 
 * 		que se almacene todos los datos en el fichero id_clientes.txt
 */
	int registrar() {
		
		printf("[%d] Recibida petición de registro.\n", getpid());	

		// Generando dos enteros aleatorios de 0 hasta 99
		int a = rand() % 100;
		int b = rand() % 100;

		printf("[%d] Estableciendo prueba %d + %d. ", getpid(), a, b);

		// Enviamos RESUELVE <a> <b>
		memset(buffer, 0, BUF);
		sprintf(buffer, "RESUELVE %d %d\n", a, b);
		write(cliente_socket, buffer, BUF);

		// RESPUESTA <c>
		memset(buffer, 0, BUF);
		read(cliente_socket, buffer, BUF);

		// Dividimos la cadena en RESPUESTA y <c>
		char *aux = strtok(buffer, " ");

		// Comprobamos si es el mensaje esperado
		if (strcmp(aux, "RESPUESTA") == 0) {
			
			// Comprobamos el valor de <c>
			int c = atoi(strtok(NULL, " "));
			
			printf(" Recibido %d, ", c); 

			// Si la suma es correcta, registramos al cliente
			if (c == a + b) {
				printf("prueba superada.\n");

				// Guardamos el numero secreto en la estructura del cliente
				lista_clientes[registrados].codigo = c;
				
				// Enviamos REGISTRADO OK <id>
				enviar_id();

				// Guardamos al cliente en el fichero junto con los demas clientes leidos
				guardar_clientes();
				
				return 1;
				
			}
			// En caso contrario, devolvemos mensaje de error y cerramos la conexion con el cliente
			else {
				printf("prueba NO superada\n");
				memset(buffer, 0, BUF);
				sprintf(buffer, "REGISTRADO ERROR\n");
				write(cliente_socket, buffer, BUF);
			}
		} 
		else // En caso de que la conexion se ha interrumpido
			printf("[%d] Error de conexion\n", getpid());
		
		return 0;
	}

/*
 * crear_cliente(): Manejador del proceso REGISTRA
 */
	void crear_cliente() {

			// Si ha llegado al limite, no permite mas registros
			if (registrados >= MAX) {
				printf("Alcanzado el numero maximo de clientes registrados\n");
				cerrar();
			}
				
			// Iniciar el proceso de registro
			if (!registrar(cliente_socket)) {
				cerrar();
			}
	}