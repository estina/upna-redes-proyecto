
### Grupo: RC - 48                           
                                         
#### Autores:           
##### * Eugeniu Stina     
##### * David Soto Arbizu      


# EVALUACION

Fecha: lun abr 15 00:02:12 CEST 2019

COMMIT: 77630bce117b25caa0c17d82a9cbbb3fa80cbab1

## PRACTICA 1



### Objetivo: programa.c

Compila.

Ejecuta: timeout 10s ./programa

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: make programa

Construye: make programa

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: make programa2

Construye: make programa2

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: todos

Construye: make todos

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: repite.c

Ejecuta: timeout 10s ./repite 3 Hola

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: escribe.c

Ejecuta: timeout 10s ./escribe /tmp/tmp.R0L4BJ0Dot 3

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)

## PRACTICA 2

Comprobando que se ha creado un archivo README.md en la ra
�z del repositorio





### Objetivo: README.md

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: make crear

Construye: make crear

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: envia.c

Ejecuta: timeout 10s ./envia 127.0.0.1 29478 HolaMundo

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: Datos enviados

Se lee: HolaMundo

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: make borrar

Construye: make borrar

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)

## PRACTICA 3



### Objetivo: make

Construye: make 

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: Objetivo 1

Arrancando servidor en puerto 5068

Se pudo conectar con el servidor

LINE >>> RESUELVE 16 98

Recibido RESUELVE

16 + 98 = 114

Recibido: REGISTRADO OK dewv331

Registrado

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: Objetivo 2

Arrancando servidor en puerto 8765

Se pudo conectar con el servidor

LOGIN dewv331 114

LINE: LOGIN OK

Login OK recibido.

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: Objetivo 3

Arrancando servidor en puerto 25134

Se pudo conectar con el servidor

LOGIN dewv331 114

LINE: LOGIN OK

Login OK recibido.

Recibido GETNAME

GETNAME devuelve el valor esperado.

Se envía SETNAME Asier

Recibido SETNAME OK

Recibido GETNAME

GETNAME devuelve el valor esperado.

Arrancando servidor en puerto 18686

Se pudo conectar con el servidor

LOGIN dewv331 114

LINE: LOGIN OK

Login OK recibido.

Recibido GETNAME

GETNAME devuelve el valor esperado.

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)



### Objetivo: make clean

Construye: make clean

![OK](https://dl1.cbsistatic.com/i/r/2016/07/29/d6510663-e827-4b69-933e-62708d619c99/thumbnail/32x32/102438c8e704b094eaa3034d73cba8cf/imgingest-6567147764516009122.gif)